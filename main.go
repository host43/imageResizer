//go:generate goversioninfo -manifest=imageResizer.exe.manifest
//!!!
package main

import (
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/nfnt/resize"
)

type Config struct {
	XMLName        xml.Name `xml:"config"`
	SourceDir      string   `xml:"sourceDirectory"`
	DestinationDir string   `xml:"destinationDirectory"`
	MaxDays        uint     `xml:"maxDays"`
	ErrorLog       string   `xml:"errorLog"`
	LogFile        string   `xml:"log"`
}

var (
	src           = "in"
	dst           = "out"
	w             = 1500
	h             = 1500
	errorFileFile = "errorFile.log"
	sep           = string(os.PathSeparator)
	configName    = "config.xml"
	config        = &Config{}
	errLog        = &log.Logger{}
	lg            = &log.Logger{}
	update        bool
	wtitle        bool
	otitle        bool
)

func init() {
	data, err := ioutil.ReadFile(configName)
	if err != nil {
		log.Println(err)
		os.Exit(-1)
	}
	err = xml.Unmarshal(data, config)
	if err != nil {
		log.Println(err)
		os.Exit(-1)
	}
	config.SourceDir = strings.ToLower(config.SourceDir)
	config.DestinationDir = strings.ToLower(config.DestinationDir)

	flag.BoolVar(&update, "update", false, "only new files")
	flag.BoolVar(&wtitle, "withtitle", false, "")
	flag.BoolVar(&otitle, "onlytitle", false, "")
	flag.Parse()
}

type imageFile struct {
	img      image.Image
	filename string
	count    string
}

func main() {

	//Set logging
	//errorFile, err := os.OpenFile(config.ErrorLog, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	errorFile, err := os.Create(config.ErrorLog)
	if err != nil {
		log.Panic(err)
	}
	defer errorFile.Close()
	errLog.SetOutput(errorFile)
	errLog.Println("\n*****Logging started at", time.Now().Format("2006-01-02 15:04:05"))
	//here we are trying to write panic error to error.log.
	defer func() {
		if r := recover(); r != nil {
			errLog.Println(r)
			os.Exit(-1)
		}
	}()

	//logFile, err := os.OpenFile(config.LogFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	logFile, err := os.Create(config.LogFile)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()
	lg.SetOutput(logFile)
	lg.Println("\n*****Logging started at", time.Now().Format("2006-01-02 15:04:05"))
	// logging

	// catch interrupt signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs)
	go func() {
		sig := <-sigs
		fmt.Println(sig)
		errLog.Println(sig)
		errLog.Println("Program was terminated by user at: ", time.Now())
		os.Exit(-1)
	}()
	// interrupt
	root, err := filepath.Glob(config.SourceDir + sep + "*")
	if err != nil {
		errLog.Println(config.SourceDir+": ", err)
		os.Exit(-1)
	}
	for _, entry1 := range root { //"in/*"\
		entry1 := strings.ToLower(entry1) //added 20180730
		if err := isDir(entry1); err != nil {
			errLog.Println(entry1+": ", err)
			continue
		}
		err = os.MkdirAll(outDir(entry1), 0777)
		if err != nil {
			errLog.Println(outDir(entry1)+": ", err)
		}
		//if entry is not a dir then out log message and continue
		level1, err := filepath.Glob(entry1 + sep + "*")
		if err != nil {
			errLog.Println(entry1+": ", err)
			continue
		}
		for _, entry2 := range level1 { //"in/_0012/*"
			entry2 := strings.ToLower(entry2) //added 20180730
			if err := isDir(entry2); err != nil {
				errLog.Println(entry2+": ", err)
				continue
			}
			err = os.MkdirAll(outDir(entry2), 0777)
			if err != nil {
				errLog.Println(outDir(entry2)+": ", err)
			}
			if isEbayTitle(entry2) {
				err := doOnce(entry2, "", "titel.jpg")
				if err != nil {
					errLog.Println(entry2+": ", err)
				}
				continue
			}
			level2, err := filepath.Glob(entry2 + sep + "*")
			if err != nil {
				errLog.Println(entry2+": ", err)
				continue
			}
			for _, entry3 := range level2 { //"in/_0012/0001/*"
				entry3 := strings.ToLower(entry3)
				if err := isDir(entry3); err != nil {
					errLog.Println(entry3+": ", err)
					continue
				}
				err = os.MkdirAll(outDir(entry3), 0777)
				if err != nil {
					errLog.Println(outDir(entry3)+": ", err)
				}
				if isMatchedDir(entry3) {
					if isTitelDir(entry3) {
						if err := doOnce(entry3, "", "titel.jpg"); err != nil {
							errLog.Println(entry3+": ", err)
						}
						continue
					}
					files, err := filepath.Glob(entry3 + sep + "*")
					if err != nil {
						errLog.Println(entry3+": ", err)
						continue
					}
					sort.Strings(files)
					flag := !update
					if update && newFilesExist(files) {
						flag = true
					}
					if flag {
						i := 0
						for _, file := range files {
							file = strings.ToLower(file)
							if isJpeg(file) {
								resized, err := doResize(file)
								if err != nil {
									errLog.Println(file+": ", err)
									continue
								}
								i = i + 1
								err = writeOut(file, resized, strconv.Itoa(i)+".jpg")
								if err != nil {
									errLog.Println(err)
									errLog.Println("'" + file + "' was not resized")
									//lg.Println("'" + file + "' was not resized")
								}
								continue
							} else {
								errLog.Println("'" + file + "' is not JPEG")
							}
						}
					}
				}
			}
		}
	}
	lg.Println("Exiting at ", time.Now().Format("2006-01-02 15:04:05"))
}
func newFilesExist(files []string) bool {
	for _, file := range files {
		fh, err := os.Open(file)
		if err != nil {
			return false
		}
		defer fh.Close()
		finfo, err := fh.Stat()
		if err != nil {
			return false
		}
		modTime := finfo.ModTime()
		maxSecs := 60 * 60 * 24 * config.MaxDays
		if maxSecs > uint(time.Now().Unix()-modTime.Unix()) {
			return true
		}
	}
	return false
}

func isMatchedDir(p string) bool {
	last := lastPart(p)
	if otitle {
		return isTitelDir(p)
	}
	return last != "photographed" && last != "3d images"
}

func isMatchedFile(p string) bool {
	if update {
		fh, err := os.Open(p)
		if err != nil {
			return false
		}
		defer fh.Close()
		finfo, err := fh.Stat()
		if err != nil {
			return false
		}
		modTime := finfo.ModTime()
		return config.MaxDays*24 > uint(time.Since(modTime).Hours())-config.MaxDays*24
	}
	return true
}

func isJpeg(p string) bool {
	ext := path.Ext(p)
	return ext == ".jpg" || ext == ".jpeg"
}

func isEbayTitle(p string) bool {
	last := lastPart(p)
	return last == "ebay_title"
}

func isTitelDir(p string) bool {
	last := lastPart(p)
	return last == "titel"
}

func doOnce(p, n1, n2 string) error {
	entry, err := filepath.Glob(p + sep + "*")
	if err != nil {
		return err
	}
	for i := 0; i < len(entry); i++ {
		entry[i] = strings.ToLower(entry[i])
		if !isJpeg(entry[i]) { // exclude is not JPEG files
			entry = append(entry[:i], entry[i+1:]...)
			i--
		}
		//if lastPart(f) == "thumbs.db" {
		//	entry = append(entry[:i], entry[i+1:]...)
		//	break
		//}
	}
	if len(entry) != 1 { //more than one file. Out log message and skip directory. Doing nothing
		return errors.New("there is not single file")
	}
	if wtitle || isMatchedFile(entry[0]) {
		resized, err := doResize(entry[0])
		if err != nil {
			return err
		}
		err = writeOut(p+sep+n2, resized, "")
		if err != nil {
			return err
		}
	}
	return nil
}

func lastPart(p string) string {
	parts := strings.Split(p, sep)
	last := parts[len(parts)-1]
	return last
}

func filenameInsert(p, s string) string {
	ext := filepath.Ext(p)
	name := strings.TrimSuffix(p, ext)
	return name + s + ext
}

func doResize(p string) (*image.Image, error) {
	file, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	fInfo, _ := file.Stat()
	lg.Printf("---\nProcessing: %s. Last modified: %s\n", p, fInfo.ModTime().Format("2006-01-02 15:04:05"))
	srcImg, err := jpeg.Decode(file)
	if err != nil {
		return nil, err
	}
	b := srcImg.Bounds()
	resized := resize.Resize(uint(w), 0, srcImg, resize.NearestNeighbor)
	if b.Max.Y != b.Max.X {
		resized, err = insertImage(resized)
		if err != nil {
			return nil, err
		}
	}
	return &resized, nil
}

func doResize2(inImg chan imageFile, outImgCh chan imageFile) {
	for img := range inImg {
		file, err := os.Open(img.filename)
		if err != nil {
			errLog.Println(err)
			continue
		}
		defer file.Close()
		fInfo, _ := file.Stat()
		lg.Printf("---\nProcessing: %s. Last modified: %s\n", img.filename, fInfo.ModTime().Format("2006-01-02 15:04:05"))
		//dc, t, err := image.DecodeConfig(reader)
		//fmt.Println(t)
		//if err != nil {
		//	errLog.Println(err)
		//	continue
		//}
		//if correct && dc.Height == dc.Width {
		//	errLog.Println(errors.New("Running with --nosquared flag but image is squared"))
		//	continue
		//}
		srcImg, err := jpeg.Decode(file)
		if err != nil {
			errLog.Println(err)
			continue
		}
		b := srcImg.Bounds()
		img.img = resize.Resize(uint(w), 0, srcImg, resize.NearestNeighbor)
		if b.Max.Y != b.Max.X {
			img.img, err = insertImage(img.img)
			if err != nil {
				errLog.Println(err)
				continue
			}
		}
		outImgCh <- img
	}
	close(outImgCh)
}

func writeOut(p string, img *image.Image, name string) error {
	p = outDir(p)
	pathParts := strings.Split(p, sep)
	filename := pathParts[len(pathParts)-1]
	if name != "" {
		filename = filenameInsert(name, "_1500")
	} else {
		filename = filenameInsert(filename, "_1500")
	}
	pathParts[len(pathParts)-1] = filename
	p = strings.Join(pathParts, sep)
	file, err := os.Create(p)
	if err != nil {
		return errors.New("'" + p + "': " + err.Error())
	}
	defer file.Close()
	lg.Println("Writing resized file: ", p, " at: ", time.Now().Format("2006-01-02 15:04:05"))
	err = jpeg.Encode(file, *img, nil) // third parameter is image.Option can be used for change image quality
	if err != nil {
		return errors.New("'" + p + "': " + err.Error())
	}
	return nil
}

func outDir(p string) string {
	return strings.Replace(p, config.SourceDir, config.DestinationDir, 1)
}

func todayDate() string {
	now := time.Now()
	return now.Format("2006-01-02")
}

func isDir(p string) error {
	fh, err := os.Open(p)
	if err != nil {
		errLog.Println(err)
	}
	defer fh.Close()
	finfo, err := fh.Stat()
	if err != nil {
		return err
	}
	if !finfo.IsDir() {
		return errors.New(p + " is not directory. Skipped.")
	}
	return nil
}

func insertImage(img image.Image) (image.Image, error) {
	rect := image.Rectangle{}
	dst := image.NewRGBA(image.Rect(0, 0, w, h))
	b := img.Bounds()
	draw.Draw(dst, image.Rect(0, 0, w, h), &image.Uniform{color.RGBA{255, 255, 255, 255}}, image.ZP, draw.Src) //white image 1500x1500
	if b.Max.X > b.Max.Y {
		yo := (h - b.Max.Y) / 2
		rect = image.Rect(0, yo, w, h-yo)
	} else {
		xo := (w - b.Max.X) / 2
		rect = image.Rect(xo, 0, w-xo, w)
	}
	draw.Draw(dst, rect, img, image.ZP, draw.Src)
	return dst, nil
}
