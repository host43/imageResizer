//go:generate goversioninfo -manifest=updater.exe.manifest
package main

import (
	"bufio"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/djherbis/times"
	"github.com/nfnt/resize"
)

type Config struct {
	XMLName        xml.Name `xml:"config"`
	SourceDir      string   `xml:"sourceDirectory"`
	DestinationDir string   `xml:"destinationDirectory"`
	MaxDays        int      `xml:"maxDays"`
	HardUpdateFile string   `xml:"hardUpdateFile"`
	ErrorLog       string   `xml:"errorLog"`
	LogFile        string   `xml:"log"`
}

var (
	src        = "in"
	dst        = "out"
	w          = 1500
	h          = 1500
	configName = "config.xml"
	config     = &Config{}
	sep        = string(os.PathSeparator)
	logPath    = ""
	errLog     = &log.Logger{}
	lg         = &log.Logger{}
	hardUpdate = make(map[string]bool)
)

func init() {
	flag.StringVar(&configName, "c", "config.xml", "config file. full path")
	flag.StringVar(&logPath, "l", "", "path to logs")
	flag.Parse()

	data, err := ioutil.ReadFile(configName)
	if err != nil {
		log.Println(err)
		os.Exit(-1)
	}
	err = xml.Unmarshal(data, config)
	if err != nil {
		log.Println(err)
		os.Exit(-1)
	}
	config.SourceDir = strings.ToLower(config.SourceDir)
	config.DestinationDir = strings.ToLower(config.DestinationDir)
}

func main() {
	errorFile, err := os.OpenFile(logPath+sep+config.ErrorLog, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		log.Panic(err)
	}
	defer errorFile.Close()
	errLog.SetOutput(errorFile)
	errLog.Println("\n*****Logging started at", time.Now().Format("2006-01-02 15:04:05"))

	//we are trying to write panic error to error.log here.
	defer func() {
		if r := recover(); r != nil {
			errLog.Println(r)
			os.Exit(-1)
		}
	}()

	logFile, err := os.OpenFile(logPath+sep+config.LogFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	//logFile, err := os.Create(config.LogFile)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()
	lg.SetOutput(logFile)
	lg.Println("\n*****Logging started at", time.Now().Format("2006-01-02 15:04:05"))
	// logging

	// catch interrupt signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs)
	go func() {
		sig := <-sigs
		fmt.Println(sig)
		errLog.Println(sig)
		errLog.Println("Program was terminated by user at: ", time.Now())
		os.Exit(-1)
	}()
	// interrupt

	hardUpdate, err = getHardUpdate(config.HardUpdateFile)
	if err != nil {
		errLog.Println("Can not open hardUpdate file", err)
		return
	}
	eu, err := doUpdate()
	if err != nil {
		errLog.Println(err)
	}
	_ = eu
	hardUpdate = doHardUpdate(hardUpdate) //do hardupdate and get new list
	for k, _ := range eu {
		hardUpdate[k] = true
	}
	fh, err := os.Create(config.HardUpdateFile)
	if err != nil {
		errLog.Println(err)
	}
	defer fh.Close()
	for k, _ := range hardUpdate {
		fh.WriteString(k + "\n")
	}

	fmt.Println("update was successful )")
	lg.Println("update was successful )")
}

func doHardUpdate(hardUpdate map[string]bool) map[string]bool {
	for p, _ := range hardUpdate {
		ok := true
		err := os.MkdirAll(outDir(p), 0777)
		if err != nil {
			errLog.Println(p, ":", err)
			ok = false
			continue
		}
		if onlyOnceDir(p) {
			if err = doOnce(p, "titel.jpg"); err != nil {
				errLog.Println(p, ":", err)
				ok = false
			}
			if ok {
				delete(hardUpdate, p)
			}
			continue
		}
		if err := os.MkdirAll(p, 0777); err != nil {
			errLog.Println(p, err)
			ok = false
		}
		files, err := filepath.Glob(p + sep + "*")
		if err != nil {
			errLog.Println(p, ":", err)
			ok = false
			continue
		}
		sort.Strings(files)
		i := 0
		for _, file := range files {
			file = strings.ToLower(file)
			if isJpeg(file) {
				resized, err := doResize(file)
				if err != nil {
					errLog.Println(file, ": ", err)
					ok = false
					continue
				}
				i = i + 1
				err = writeOut(file, resized, strconv.Itoa(i)+".jpg")
				if err != nil {
					ok = false
					errLog.Println(err)
					errLog.Println(file, ": was not resized")
				}
				continue
			} else {
				//	ok = false
				errLog.Println(file, ": is not JPEG")
			}
		}
		if ok {
			delete(hardUpdate, p)
		}
	}
	return hardUpdate
}

func doUpdate() (map[string]bool, error) {
	hardUpdate := make(map[string]bool)
	deep1, err := filepath.Glob(config.SourceDir + sep + "*")
	if err != nil {
		return hardUpdate, errors.New("Root source error: " + err.Error())
	}
	for _, item := range deep1 {
		item = strings.ToLower(item)
		if err := isDir(item); err != nil { // is not directory
			errLog.Println(item, ":", err)
			continue
		}
		deep2, err := filepath.Glob(item + sep + "*")
		if err != nil {
			errLog.Println(item, ":", err)
		}
		for _, item := range deep2 {
			item = strings.ToLower(item)
			if err := isDir(item); err != nil { // is not directory
				errLog.Println(item, ":", err)
				continue
			}
			if onlyOnceDir(item) && newFilesExist(item) {
				if err = doOnce(item, "titel.jpg"); err != nil {
					hardUpdate[item] = true
					errLog.Println(item, ":", err)
				}
				continue
			}
			deep3, err := filepath.Glob(item + sep + "*")
			if err != nil {
				errLog.Println(item, ":", err)
			}
			for _, item := range deep3 {
				item = strings.ToLower(item)
				if onlyOnceDir(item) && newFilesExist(item) {
					if err = doOnce(item, "titel.jpg"); err != nil {
						hardUpdate[item] = true
						errLog.Println(item, ":", err)
					}
					continue
				}
				if isMatchedDir(item) && newFilesExist(item) {
					lg.Println("Reading directory:", item)
					if err := os.MkdirAll(outDir(item), 0777); err != nil {
						errLog.Println(item, err)
						hardUpdate[item] = true
					}
					files, err := filepath.Glob(item + sep + "*")
					if err != nil {
						errLog.Println(item, ":", err)
					}
					sort.Strings(files)
					i := 0
					for _, file := range files {
						file = strings.ToLower(file)
						if isJpeg(file) {
							resized, err := doResize(file)
							if err != nil {
								errLog.Println(file+": ", err)
								hardUpdate[item] = true
								continue
							}
							i = i + 1
							err = writeOut(file, resized, strconv.Itoa(i)+".jpg")
							if err != nil {
								errLog.Println(err)
								errLog.Println("'" + file + "' was not resized")
								hardUpdate[item] = true
							}
						} else {
							errLog.Println(file, " is not JPEG")
							//hardUpdate[item] = true
						}
					}
				}
			}
		}
	}
	return hardUpdate, nil
}

func getHardUpdate(filename string) (map[string]bool, error) {
	fh, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	hu := make(map[string]bool)
	reader := bufio.NewScanner(fh)
	reader.Split(bufio.ScanLines)
	for reader.Scan() {
		p := reader.Text()
		p = strings.ToLower(p)
		hu[p] = true
	}
	return hu, nil
}

func newFilesExist(p string) bool {
	files, err := filepath.Glob(p + sep + "*")
	if err != nil {
		errLog.Println(p, ":", err)
		return false
	}
	for _, file := range files {
		t, err := times.Stat(file)
		if err != nil {
			errLog.Println(err)
		}
		//at := t.AccessTime()
		ct := t.ChangeTime()
		bt := t.BirthTime()
		//older := at.AddDate(0, 0, config.MaxDays)
		//if time.Now().Sub(older) < 0 {
		//	return true
		//}
		older := ct.AddDate(0, 0, config.MaxDays)
		if time.Now().Sub(older) < 0 {
			return true
		}
		older = bt.AddDate(0, 0, config.MaxDays)
		if time.Now().Sub(older) < 0 {
			return true
		}
	}
	return false
}

func isDir(p string) error {
	fh, err := os.Open(p)
	if err != nil {
		errLog.Println(err)
	}
	defer fh.Close()
	finfo, err := fh.Stat()
	if err != nil {
		return err
	}
	if !finfo.IsDir() {
		return errors.New(p + " is not directory. Skipped.")
	}
	return nil
}

func isMatchedDir(p string) bool {
	last := lastPart(p)
	return last != "photographed" && last != "3d images"
}

func onlyOnceDir(p string) bool {
	last := lastPart(p)
	return last == "ebay_title" || last == "titel"
}

func doOnce(p, n string) error {
	entry, err := filepath.Glob(p + sep + "*")
	if err != nil {
		return err
	}
	for i := 0; i < len(entry); i++ {
		entry[i] = strings.ToLower(entry[i])
		if !isJpeg(entry[i]) { // exclude not JPEG files
			entry = append(entry[:i], entry[i+1:]...)
			i--
		}
	}
	if len(entry) == 0 {
		return errors.New("there are not any files")
	}
	if len(entry) != 1 { //more than one file. Out log message and skip directory. Doing nothing
		return errors.New("there is more than one file")
	}
	resized, err := doResize(entry[0])
	if err != nil {
		return err
	}
	err = writeOut(p+sep+n, resized, "")
	if err != nil {
		return err
	}
	return nil
}

func isJpeg(p string) bool {
	ext := path.Ext(p)
	return ext == ".jpg" || ext == ".jpeg"
}

func doResize(p string) (*image.Image, error) {
	file, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	fInfo, _ := file.Stat()
	lg.Printf("Processing: %s. Last modified: %s\n", p, fInfo.ModTime().Format("2006-01-02 15:04:05"))
	srcImg, err := jpeg.Decode(file)
	if err != nil {
		return nil, err
	}
	b := srcImg.Bounds()
	resized := resize.Resize(uint(w), 0, srcImg, resize.NearestNeighbor)
	if b.Max.Y != b.Max.X {
		resized, err = insertImage(resized)
		if err != nil {
			return nil, err
		}
	}
	return &resized, nil
}

func writeOut(p string, img *image.Image, name string) error {
	p = outDir(p)
	pathParts := strings.Split(p, sep)
	filename := pathParts[len(pathParts)-1]
	if name != "" {
		filename = filenameInsert(name, "_1500")
	} else {
		filename = filenameInsert(filename, "_1500")
	}
	pathParts[len(pathParts)-1] = filename
	p = strings.Join(pathParts, sep)
	file, err := os.Create(p)
	if err != nil {
		return errors.New(p + ":" + err.Error())
	}
	defer file.Close()
	lg.Println("Writing resized file: ", p, " at: ", time.Now().Format("2006-01-02 15:04:05"))
	err = jpeg.Encode(file, *img, nil) // third parameter is image.Option can be used for change image quality
	if err != nil {
		return errors.New(p + ":" + err.Error())
	}
	return nil
}

func outDir(p string) string {
	return strings.Replace(p, config.SourceDir, config.DestinationDir, 1)
}

func insertImage(img image.Image) (image.Image, error) {
	rect := image.Rectangle{}
	dst := image.NewRGBA(image.Rect(0, 0, w, h))
	b := img.Bounds()
	draw.Draw(dst, image.Rect(0, 0, w, h), &image.Uniform{color.RGBA{255, 255, 255, 255}}, image.ZP, draw.Src) //white image 1500x1500
	if b.Max.X > b.Max.Y {
		yo := (h - b.Max.Y) / 2
		rect = image.Rect(0, yo, w, h-yo)
	} else {
		xo := (w - b.Max.X) / 2
		rect = image.Rect(xo, 0, w-xo, w)
	}
	draw.Draw(dst, rect, img, image.ZP, draw.Src)
	return dst, nil
}

func lastPart(p string) string {
	parts := strings.Split(p, sep)
	last := parts[len(parts)-1]
	return last
}

func filenameInsert(p, s string) string {
	ext := filepath.Ext(p)
	name := strings.TrimSuffix(p, ext)
	return name + s + ext
}
